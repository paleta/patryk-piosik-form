import * as React from 'react';

import Button from '../UI/Button/Button';
import './Notification.css';

import ok from '../../assets/images/ok.png';
import warning from '../../assets/images/warning.png';

const notification = (props: any) => {


    let message = null;

    if (props.type === 0) {

        message = (
            <div className="notification__message">
                <img 
                    className="notification__icon"
                    src={warning}/>
                <p className="notification__text">{props.message}</p>
            </div>);

    } else if (props.type === 1) {

        message = (
            <div className="notification__message">
                <img 
                    className="notification__icon"
                    src={ok}/>
                <p className="notification__text">{props.message}</p>
            </div>);

    }



    return (
        <div className="notification">
            <Button 
                type="button"
                clicked={props.clicked}
                disabled={props.disabled}
                classes="notification__btn"><span>+</span> Add user</Button>
                {message}
        </div>
    );
};
export default notification;