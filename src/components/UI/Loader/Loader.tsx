import * as React from 'react';
import './Loader.css';
const loader = () => {
    return (
        <div className="lds-heart"><div /></div>
    );
};
export default loader;