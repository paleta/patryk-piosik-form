import * as React from 'react';

import './Input.css';

const input = (props: any) => {

    const inputClass = ['input'];

    if (props.classes) {
        inputClass.push(props.classes);
    }

    if (props.invalid && props.touched) {
        inputClass.push('input--invalid');
    } 

    return (
        <input
            autoFocus={props.focus}
            type={props.type}
            placeholder={props.placeholder}
            className={inputClass.join(' ')}
            value={props.value}
            onChange={props.changed}/>
    );
};
export default input;