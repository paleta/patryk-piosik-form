import * as React from 'react';
import './Button.css';


const button = (props: any) => {

    const btnClass = ['btn'];

    if (props.classes) {
        btnClass.push(props.classes);
    }

    return (
        <button
            disabled={props.disabled}
            type={props.type}
            onClick={props.clicked}
            className={btnClass.join(' ')}>{props.children}</button>
    );
};
export default button;