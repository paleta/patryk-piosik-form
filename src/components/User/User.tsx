import * as React from 'react';

import './User.css';


interface IUser {
    lp: number;
    name: string;
    email: string; 
    clicked: () => void
}


const user = (props: IUser) => {
    return (
        <li className="user">
            <div className="user__column">{props.lp}</div>
            <div className="user__column user__column--second">{props.name}</div>
            <div className="user__column user__column--thrid">{props.email}</div>
            <div className="user__controls">
                <button 
                    onClick={props.clicked}
                    className="user__btn-remove">&#x2716;</button>
            </div>
        </li>
    );
};
export default user;