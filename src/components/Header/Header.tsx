import * as React from 'react';
import logo from '../../assets/images/logo.png';
import './Header.css';

const header = () => {
    return (
        <header className="header">
            <div className="header__content">
                <div className="header__logo">
                    <img src={logo} alt="Patryk Piosik logo"/>
                </div>
                <div className="u-fill-remaning-space"/>
                <div className="header__controls">
                    <a 
                        className="header__link" 
                        href="https://www.piosik.xyz"
                        target="_blank">www.piosik.xyz</a>
                </div>    
            </div>
        </header>
    );
};
export default header;