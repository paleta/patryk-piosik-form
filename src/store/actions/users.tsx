import { store } from '../../index';
import { httpObservable } from '../../shared/utilities';

import { 
    FETCH_USERS_FAIL, 
    FETCH_USERS_START, 
    GET_USERS_SUCCESS, 
    HIDE_FORM,
    SHOW_FORM,
} from '../actionTypes';

import { IAction, IUser } from '../reducers/users';



// FETCHING USERS

export const fetchStart = (): IAction => {
    return {
        type: FETCH_USERS_START
    };
};


export const fetchFail = (error: any): IAction => {
    return {
        payload: error,
        type: FETCH_USERS_FAIL
    };
};

// GET USERS

export const getUsersSuccess = (users: IUser[]): IAction => {
    return {
        payload: users,
        type: GET_USERS_SUCCESS
    };
};

export const getUsers = () => {
    return (dispatch: any) => {
        dispatch(fetchStart());

        httpObservable('/users')
            .subscribe((data: IUser[]) => { 

                if (data.length >= 10) {
                    dispatch(hideForm('You can\'t add new user because of a limit', 0)); 
                } else {
                    dispatch(hideForm('You have successfully fetched users', 1));    
                }  

                dispatch(getUsersSuccess(data));
            }, (error: Error) => {
                dispatch(hideForm(error.message, 0)); 
                dispatch(fetchFail(error));
            });
    };
};

// SHOW FORM
export const showForm = (): IAction => {
    return {
        type: SHOW_FORM
    }
}

// HIDE FORM
export const hideForm = (message: string, type: number): IAction => {
    return {
        payload: {
            message,
            type
        },
        type: HIDE_FORM
    }
}

// ADD USER

export const addUser = (user: IUser) => {
    return (dispatch: any) => {
        dispatch(fetchStart());

        const usersArray = [
            ...store.getState().users.data
        ]; 

        const checkIfEmailExist = usersArray.find(usr => usr.email.toLowerCase() === user.email.toLowerCase());

        if (!!checkIfEmailExist) {
            dispatch(hideForm('Wait a minute.. Seems this email already exist', 0)); 
            return dispatch(getUsersSuccess(usersArray));
        }
        
        httpObservable('/users', 'POST', user)
            .subscribe((newUserFromResponse: IUser) => {

                const newUser = newUserFromResponse;
    
                if (usersArray.length !== 0) {
                    const usersID: number[] = []; 
                    usersArray.forEach((userEl: IUser) => {
                        usersID.push(userEl.id);
                    });
        
                    const index = Math.max(...usersID) + 1;
                    newUser.id = index;
                } 

                usersArray.push(newUser);

                if (usersArray.length >= 10) {
                    dispatch(hideForm('You can\'t add new user because of a limit', 0)); 
                } else {
                    dispatch(hideForm('You have successfully added an user', 1)); 
                }

                dispatch(getUsersSuccess(usersArray));
                dispatch(sortUsers());

            }, (error: Error) => {
                dispatch(hideForm(error.message, 0)); 
                dispatch(fetchFail(error));
            });
    };
};

// DELETE USER

export const deleteUser = (id: number) => {
    return (dispatch: any) => {
        dispatch(fetchStart());
        
        httpObservable('/users/' + id, 'DELETE')
            .subscribe(() => {
                const usersArray = store.getState().users.data.filter(user => user.id !== id);
                dispatch(hideForm('You have successfully deleted an user', 1)); 
                dispatch(getUsersSuccess(usersArray));
            }, (error: Error) => {
                dispatch(hideForm(error.message, 0)); 
                dispatch(fetchFail(error));
            });
    };
};


// SORT USERS 

export const sortUsers = (sortBy: string = 'id', order: string = 'asc') => {
    return (dispatch: any) => {

        const usersArray = [
            ...store.getState().users.data
        ];

        usersArray.sort((a, b) => {

            let itemA = a[sortBy];
            let itemB = b[sortBy];

            if (sortBy !== 'id') {            
                itemA = itemA.toLowerCase();
                itemB = itemB.toLowerCase();
            }

            switch (order) {
                case 'desc':
                    if (itemA < itemB) {
                        return 1;
                    }
                    break;
                case 'asc':
                    if (itemA > itemB) {
                        return 1;
                    }
                    break;
                default:
                    break;
            }

            return 0; 
           });
        
        dispatch(getUsersSuccess(usersArray));
    };
};
