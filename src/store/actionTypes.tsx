export const FETCH_USERS_START = 'FETCH_USERS_START';
export const FETCH_USERS_FAIL = 'FETCH_USERS_FAIL';

export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';

export const SHOW_FORM = 'SHOW_FORM';
export const HIDE_FORM = 'HIDE_FORM';

export const SORT_USERS = 'SORT_USERS';

