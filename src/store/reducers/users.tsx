import { 
    FETCH_USERS_FAIL, 
    FETCH_USERS_START, 
    GET_USERS_SUCCESS,
    HIDE_FORM, 
    SHOW_FORM,
} from '../actionTypes';


export interface IUser {
    id: number;
    name: string;
    email: string;   
}

export interface INotification {
    type: number | null;
    message: string;
}

export interface IUsersSate {
    busy: boolean;
    data: IUser[];
    error: any;
    notification: INotification;
    showForm: boolean;
}

export interface IAction {
    payload?: any;
    type: string;
}

const initialState = {
    busy: false,
    data: [], 
    error: null,
    notification: {
        message: '',
        type: null,
    },
    showForm: false,
};

const usersReducer = (state: IUsersSate = initialState, action: IAction): IUsersSate => {

    switch (action.type) {
        case FETCH_USERS_START:
            return {
                ...state,
                busy: true,
                error: null,
                notification: {
                    message: '',
                    type: null,
                }
            };   
        case GET_USERS_SUCCESS:
            return {
                ...state,
                busy: false,
                data: action.payload,             
                error: null
            };  
        case FETCH_USERS_FAIL:
            return {
                ...state,
                busy: false,
                error: action.payload                
            };   
        case SHOW_FORM:
            return {
                ...state,
                notification: {
                    message: '',
                    type: null,
                },        
                showForm: true,
            };  
        case HIDE_FORM:
            return {
                ...state,
                notification: action.payload,      
                showForm: false,  
            };                   
        default:
            return state;
    }
};

export default usersReducer;
