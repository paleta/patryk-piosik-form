import * as React from 'react';

import Header from '../../components/Header/Header';
import Aux from '../_Aux/_Aux';
import './Layout.css';

class Layout extends React.Component {


    public render () {
        return (
            <Aux>
                <div className="container">
                <Header/>
                    <main>{this.props.children}</main>
                </div>  
            </Aux> 
        );
    };


};
export default Layout;