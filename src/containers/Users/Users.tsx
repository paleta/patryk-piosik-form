import * as React from 'react'
import { connect } from 'react-redux'

import Notification from '../../components/Notification/Notification'
import Loader from '../../components/UI/Loader/Loader'
// import Button from '../../components/UI/Button/Button';
import User from '../../components/User/User'
// import Aux from '../../hoc/_Aux/_Aux';
import { 
    addUser,
    deleteUser, 
    getUsers, 
    showForm,
    sortUsers,
 } from '../../store/actions/users'
import { 
    INotification,
    IUser, 
} from '../../store/reducers/users'
import NewUserForm from '../NewUserForm/NewUserForm'
import './Users.css'

interface IUsersProps {
    onGetUsers: () => void
    onDeleteUser: (id: number) => void
    onShowForm: () => void
    onAddUser: (user: IUser) => any
    onSortUsers: (sortBy: string, order: string) => void
    notification: INotification
    users: IUser[]
    showForm: boolean
    busy: boolean
}

interface IUsersState  {
    email: {
        order: string;
    }
    id: {
        order: string;
    }
    name: {
        order: string;
    }
}

class Users extends React.Component<IUsersProps, IUsersState> {

    public state = {
        email: {
            order: 'asc'
        },     
        id: {
            order: 'asc'
        },
        name: {
            order: 'asc'
        },
    }
    
    public componentDidMount() {
        this.props.onGetUsers()
    }

    public onRemoveUserHandler = (id: number) => {
        this.props.onDeleteUser(id)
    }

    public onShowFormHandler = () => {   
        this.props.onShowForm()
    }

    public onSortUsersHandler = (sortBy: string) => {

        this.props.onSortUsers(sortBy, this.state[sortBy].order)
        this.setState(prevState => {
            return {
                ...this.state,
                [sortBy]: {
                    order: prevState[sortBy].order === 'asc' ? 'desc' : 'asc'
                }
            }
        })

    }

    
    public render () {

        let users = null

        if (this.props.users.length !== 0) {
            users = this.props.users.map((el, idx) => {
                return <User 
                            key={el.id}
                            lp={el.id}
                            name={el.name}
                            email={el.email}
                            clicked={this.onRemoveUserHandler.bind(this, el.id)}/>
            })
        }

        let controls = null

        if (this.props.showForm) {
            controls = (
                <NewUserForm/>)
        } else {
            controls = (
                <Notification
                    message={this.props.notification.message}
                    type={this.props.notification.type}
                    clicked={this.onShowFormHandler}
                    disabled={this.props.users.length >= 10}/>)
        }

        let contentToRender = null

        if (this.props.busy) {
            contentToRender = (
                <div className="users__loader">
                    <Loader/>
                </div>
            )
        } else {
            contentToRender = (
                <div className="users__table">
                    <div className="users__table-header">
                        <div 
                            onClick={this.onSortUsersHandler.bind(this, 'id')} 
                            className="users__table-column">Lp</div>
                        <div 
                            onClick={this.onSortUsersHandler.bind(this, 'name')} 
                            className="users__table-column users__table-column--second">User</div>
                        <div 
                            onClick={this.onSortUsersHandler.bind(this, 'email')}
                            className="users__table-column">E-mail</div>
                    </div>
                    <ul className="users__list">
                        {users}
                    </ul>
                </div>
            )
        }

        return (
            <div className="users">
                <div className="users__controls">
                    {controls}
                </div>
                {contentToRender}
                {(this.props.users.length === 0 && !this.props.busy) ? <div className="users__empty">There is no users</div> : null}
            </div>
        )
    };
};

const mapStateToProps = (state: any) => {
    return {
        busy: state.users.busy,
        notification: state.users.notification,
        showForm: state.users.showForm,
        users: state.users.data,
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        onAddUser: (user: IUser) => dispatch(addUser(user)),
        onDeleteUser: (id: number) => dispatch(deleteUser(id)),
        onGetUsers: () => dispatch(getUsers()),
        onShowForm: () => dispatch(showForm()),
        onSortUsers: (sortBy: string, order: string) => dispatch(sortUsers(sortBy, order)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);