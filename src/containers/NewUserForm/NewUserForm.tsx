import * as React from 'react';
import { connect } from 'react-redux';

import Button from '../../components/UI/Button/Button';
import Input from '../../components/UI/Input/Input';
import { validate } from '../../shared/utilities';
import { addUser } from '../../store/actions/users';
import { IUser } from '../../store/reducers/users';

import './NewUserForm.css';

interface INewUserFormProps {
    onAddUser: (user: IUser) => any;
}

class NewUserForm extends React.Component<INewUserFormProps> {

    public state = {
        inputs: {
            email: {
                name: 'email',
                placeholder: 'E-mail...',
                touched: false,
                valid: false,
                validation: {                                
                    email: true,
                    required: true,
                },               
                value: '',
            },
            name: {
                name: 'name',
                placeholder: 'Name...',
                touched: false,
                valid: false,
                validation: {
                    maxLength: 20, 
                    onlyLetters: true,
                    required: true,
                },
                value: ''
            }
        }
    };

    public clearInputs = () => {
        this.setState({
            inputs: {
                 email: {
                     ...this.state.inputs.email,
                     touched: false,
                     value: '',
                 },
                 name: {
                     ...this.state.inputs.name,
                     touched: false,
                     value: '',
             },
            }
         });  
    }

    public onInputChangeHandler = (inputName: string, event: any) => {
        const input = {
            ...this.state.inputs[inputName],
            touched: true,
            valid: validate(event.target.value, this.state.inputs[inputName].validation),
            value: event.target.value,
        }

        this.setState({
            inputs: {
                ...this.state.inputs,
                [inputName]: input
            }
        });  
    };

    public onInputResetHanlder = (event: React.FormEvent) => {
        event.preventDefault();
        this.clearInputs();
    };


    public onSubmitHandler = (event: React.FormEvent) => {
        event.preventDefault();
     
        if (this.state.inputs.email.valid && this.state.inputs.name.valid) {

            this.props.onAddUser({
                email: this.state.inputs.email.value,
                id: 0,
                name: this.state.inputs.name.value,
            });

            this.clearInputs();
        }
    };

    public render () {

        const inputsArray = [];

        for (const inputName in this.state.inputs) {
            if (this.state.inputs.hasOwnProperty(inputName)) {
                inputsArray.push(this.state.inputs[inputName]); 
                // areAllInputsValid = this.state.inputs[inputName].valid && areAllInputsValid;           
            }
        }
        
        const inputs = inputsArray.map(input => {
            return (
                <Input 
                    focus={input.name === 'email'}
                    touched={input.touched}
                    key={input.name}
                    type="text"
                    invalid={!input.valid}
                    value={input.value}
                    changed={this.onInputChangeHandler.bind(this, input.name)}
                    placeholder={input.placeholder}
                    classes="newUserForm__input"/>
            );
        });

        let resetInput = null;

        if (this.state.inputs.email.value !== '' || this.state.inputs.name.value !== '') {
            resetInput = (
                <Button 
                    type="button"
                    clicked={this.onInputResetHanlder}
                    classes="newUserForm__reset-btn">Reset fields</Button>);
        }

        return (        
            <form 
                className="newUserForm"
                onSubmit={this.onSubmitHandler}>
                {inputs}
                <Button 
                    type="submit"
                    classes="newUserForm__submit-btn">Submit</Button> 
                {resetInput}                  
            </form>);

    };
};


const mapDispatchToProps = (dispatch: any) => {
    return {
        onAddUser: (user: IUser) => dispatch(addUser(user)),
    };
};

export default connect(null, mapDispatchToProps)(NewUserForm);