import { Observable, Observer } from 'rxjs';

export const httpObservable = (url: string, method: string = 'GET', payload?: any): Observable<any> => {

    return Observable.create((observer: Observer<any>) => {

        fetch('https://jsonplaceholder.typicode.com' + url, {
            body: JSON.stringify(payload), 
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            method,
        })
        .then(response => response.json()) 
        .then(data => {
            observer.next(data);
            observer.complete();
        })
        .catch(err => observer.error(err));
    });
  
};

export const validate = (value: string, rules: any): boolean => {

    let valid = true;

    if (rules.required) {
        valid = value.trim() !== '' && valid;
    }
    if (rules.minLength) {
        valid = value.length >= rules.minLength && valid;
    }
    if (rules.maxLength) {
        valid = value.length <= rules.maxLength && valid;
    }
    if (rules.email) {
        const pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        valid = !!pattern.test(value) && valid;
    }
    if (rules.onlyLetters) {

        const pattern = /([\u0000-\u0040\u005B-\u0060\u007B-\u00BF\u02B0-\u036F\u00D7\u00F7\u2000-\u2BFF])+/;
        const val = value.replace(/\s/g,'');

        valid = !pattern.test(val) && valid;
    }


    return valid;
}

