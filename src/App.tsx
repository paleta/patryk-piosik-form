import * as React from 'react';
import './App.css';

import Users from './containers/Users/Users';
import Layout from './hoc/Layout/Layout';

class App extends React.Component {
  public render() {
    return (
      <Layout>
        <Users/>
      </Layout>
    );
  }
}

export default App;
